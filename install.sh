#!/usr/bin/env bash

cargo build --release

TARGET_DIR=`cargo metadata --format-version 1 | jq -r '.target_directory'`
TARGET="${TARGET_DIR}/release/mousemap"

sudo install -Dm755 ${TARGET} -t /usr/local/bin
