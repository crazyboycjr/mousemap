# Mousemap

This simple program maps the side button and extra button (button 8 and button 9) of my mouse to `Super_L` and `Shift_L + Super_L`.

Usually, this can be done by using a combination of xbindkeys and xte/xdotool/xvkbd.
However, combo keys like side button + left click will not act as `Super_L + click` and cannot be grabbed by the X server.
Thus, this program work at the layer of libevdev and uinput to achieve this goal.
