pub mod code {
    pub const KEY_SUPER_L: u16 = 125;
    pub const KEY_SHIFT_L: u16 = 42;
    pub const BTN_8: u16 = 275;
    pub const BTN_9: u16 = 276;
}

pub mod value {
    pub const KEY_SUPER_L: i32 = 458979;
    pub const KEY_SHIFT_L: i32 = 458977;
    pub const BTN_8: i32 = 589828;
    pub const BTN_9: i32 = 589829;
}

// on Super_L KeyPress
// input_event { time: timeval { tv_sec: 1616906637, tv_usec: 234973 }, type_: 4, code: 4, value: 458979 }
// input_event { time: timeval { tv_sec: 1616906637, tv_usec: 234973 }, type_: 1, code: 125, value: 1 }
// input_event { time: timeval { tv_sec: 1616906637, tv_usec: 234973 }, type_: 0, code: 0, value: 0 }
// on Super_L KeyRelease
// input_event { time: timeval { tv_sec: 1616906637, tv_usec: 327922 }, type_: 4, code: 4, value: 458979 }
// input_event { time: timeval { tv_sec: 1616906637, tv_usec: 327922 }, type_: 1, code: 125, value: 0 }
// input_event { time: timeval { tv_sec: 1616906637, tv_usec: 327922 }, type_: 0, code: 0, value: 0 }

// and this is what happens when the Super_L is pressed and not released for a while
// input_event { time: timeval { tv_sec: 1616906952, tv_usec: 951291 }, type_: 1, code: 125, value: 2 }
// input_event { time: timeval { tv_sec: 1616906952, tv_usec: 951291 }, type_: 0, code: 0, value: 1 }
// input_event { time: timeval { tv_sec: 1616906952, tv_usec: 987839 }, type_: 1, code: 125, value: 2 }
// input_event { time: timeval { tv_sec: 1616906952, tv_usec: 987839 }, type_: 0, code: 0, value: 1 }

// on button8 ButtonPress
// input_event { time: timeval { tv_sec: 1616906727, tv_usec: 656824 }, type_: 4, code: 4, value: 589828 }
// input_event { time: timeval { tv_sec: 1616906727, tv_usec: 656824 }, type_: 1, code: 275, value: 1 }
// input_event { time: timeval { tv_sec: 1616906727, tv_usec: 656824 }, type_: 0, code: 0, value: 0 }
// on button8 ButtonRelease
// input_event { time: timeval { tv_sec: 1616906727, tv_usec: 816787 }, type_: 4, code: 4, value: 589828 }
// input_event { time: timeval { tv_sec: 1616906727, tv_usec: 816787 }, type_: 1, code: 275, value: 0 }
// input_event { time: timeval { tv_sec: 1616906727, tv_usec: 816787 }, type_: 0, code: 0, value: 0 }

// and this is what happens when the button9 is pressed and not released for a while
// it seems like there is a protocol that specifies what should be the value, which is good for us
// input_event { time: timeval { tv_sec: 1616906748, tv_usec: 987759 }, type_: 1, code: 276, value: 2 }
// input_event { time: timeval { tv_sec: 1616906748, tv_usec: 987759 }, type_: 0, code: 0, value: 1 }
// input_event { time: timeval { tv_sec: 1616906749, tv_usec: 24563 }, type_: 1, code: 276, value: 2 }
// input_event { time: timeval { tv_sec: 1616906749, tv_usec: 24563 }, type_: 0, code: 0, value: 1 }

// on button9 ButtonPress
// input_event { time: timeval { tv_sec: 1616906730, tv_usec: 418836 }, type_: 4, code: 4, value: 589829 }
// input_event { time: timeval { tv_sec: 1616906730, tv_usec: 418836 }, type_: 1, code: 276, value: 1 }
// input_event { time: timeval { tv_sec: 1616906730, tv_usec: 418836 }, type_: 0, code: 0, value: 0 }
// on button9 ButtonRelease
// input_event { time: timeval { tv_sec: 1616906730, tv_usec: 572825 }, type_: 4, code: 4, value: 589829 }
// input_event { time: timeval { tv_sec: 1616906730, tv_usec: 572825 }, type_: 1, code: 276, value: 0 }
// input_event { time: timeval { tv_sec: 1616906730, tv_usec: 572825 }, type_: 0, code: 0, value: 0 }

use std::fs;
use std::io;
use std::path::Path;

use evdev_rs::enums::EventCode;
use evdev_rs::enums::EventType;
use evdev_rs::enums::EV_KEY;
use evdev_rs::enums::EV_MSC::MSC_SCAN;
use evdev_rs::enums::EV_SYN::SYN_REPORT;
use evdev_rs::Device;
use evdev_rs::InputEvent;
use evdev_rs::ReadStatus;
use evdev_rs::UInputDevice;

fn open_evdev<P: AsRef<Path>>(path: P) -> io::Result<Device> {
    let f = fs::File::open(path)?;
    let mut d = Device::new().unwrap();
    d.set_fd(f)?;
    Ok(d)
}

/// Open with device with `evdev_rs::GrabMode`. All the event will be catched by this device handle.
struct GrabDevice(Device);

impl GrabDevice {
    fn from_evdev(mut d: Device) -> io::Result<GrabDevice> {
        d.grab(evdev_rs::GrabMode::Grab)?;
        Ok(GrabDevice(d))
    }

    fn open<P: AsRef<Path>>(path: P) -> io::Result<GrabDevice> {
        let d = open_evdev(path)?;
        Self::from_evdev(d)
    }
}

impl Drop for GrabDevice {
    fn drop(&mut self) {
        self.0.grab(evdev_rs::GrabMode::Ungrab).unwrap();
    }
}

struct Keyboard {
    uinput: UInputDevice,
}

impl Keyboard {
    fn open<P: AsRef<Path>>(path: P) -> io::Result<Self> {
        let dev = open_evdev(path)?;
        let uinput = UInputDevice::create_from_device(&dev)?;
        Ok(Keyboard { uinput })
    }

    /// forward `write_event`
    #[inline]
    fn write_event(&self, event: &InputEvent) -> io::Result<()> {
        self.uinput.write_event(event)
    }
}

const DEFAULT_KEYBOARD_DIR: &str = "/dev/my_keyboard";

fn find_first_keyboard<P: AsRef<Path>>(dir: P) -> Option<Keyboard> {
    let iter = fs::read_dir(dir).unwrap_or_else(|e| panic!("fs::read_dir: {}", e));
    for entry in iter {
        let kbd = (|| {
            let entry = entry?;
            eprintln!(
                "open: device {:?}, {:?}",
                entry.file_name(),
                entry.file_type()
            );
            Keyboard::open(entry.path())
        })();
        match kbd {
            Ok(kbd) => return Some(kbd),
            Err(e) => eprintln!("{}", e),
        }
    }
    None
}

fn wait_mouse_keyboard_ready<P1: AsRef<Path>, P2: AsRef<Path>>(mouse: P1, keyboard: P2) {
    loop {
        if mouse.as_ref().exists() && keyboard.as_ref().exists() {
            break;
        }
        std::thread::sleep(std::time::Duration::from_millis(1000));
    }
}

fn main() {
    wait_mouse_keyboard_ready("/dev/my_mouse/m590", DEFAULT_KEYBOARD_DIR);

    let mouse_dev = GrabDevice::open("/dev/my_mouse/m590")
        .unwrap_or_else(|e| panic!("open mouse failed: {}", e));

    let keyboard_udev =
        find_first_keyboard(DEFAULT_KEYBOARD_DIR).expect("no usable keyboard was found");

    let mouse_udev = UInputDevice::create_from_device(&mouse_dev.0)
        .unwrap_or_else(|e| panic!("open mouse failed: {}", e));

    // after initializing those devices, daemonize the following code
    if std::env::args().find(|x| x == "--daemonize").is_some() {
        nix::unistd::daemon(false, false).unwrap_or_else(|e| panic!("daemon: {}", e));
    }

    // intercept interesting event from the mouse, modify it, then forward it to the keyboard
    let mut button8_on = false;
    let mut button9_on = false;

    let mut syn_dropped = false;
    loop {
        let flag = if syn_dropped {
            evdev_rs::ReadFlag::SYNC
        } else {
            evdev_rs::ReadFlag::NORMAL | evdev_rs::ReadFlag::BLOCKING
        };

        match mouse_dev.0.next_event(flag) {
            Err(e) if e.kind() == std::io::ErrorKind::WouldBlock => {}
            Err(e) => {
                panic!("error: {}", e)
            }
            Ok((ReadStatus::Sync, _)) => {
                syn_dropped = true;
            }
            Ok((ReadStatus::Success, ev)) => {
                syn_dropped = false;
                // println!("{:?}", ev);

                let mut key_ev = ev.clone();

                match ev {
                    InputEvent {
                        event_type: EventType::EV_MSC,
                        event_code: EventCode::EV_MSC(MSC_SCAN),
                        value: value::BTN_8,
                        ..
                    } => {
                        // map button 8 to left meta
                        button8_on = true;
                        key_ev.value = value::KEY_SUPER_L;
                        keyboard_udev.write_event(&key_ev).unwrap();
                    }
                    InputEvent {
                        event_type: EventType::EV_MSC,
                        event_code: EventCode::EV_MSC(MSC_SCAN),
                        value: value::BTN_9,
                        ..
                    } => {
                        // map button 9 to left shift + left meta
                        button9_on = true;
                        // send left shift first
                        let mut shift_ev = ev.clone();
                        shift_ev.value = value::KEY_SHIFT_L;
                        keyboard_udev.write_event(&shift_ev).unwrap();
                        // emit left super
                        key_ev.value = value::KEY_SUPER_L;
                        keyboard_udev.write_event(&key_ev).unwrap();
                    }
                    InputEvent {
                        event_type: EventType::EV_KEY,
                        event_code: EventCode::EV_KEY(EV_KEY::BTN_SIDE),
                        value,
                        ..
                    } if value <= 1 => {
                        assert!(button8_on);
                        key_ev.event_code = EventCode::EV_KEY(EV_KEY::KEY_LEFTMETA);
                        keyboard_udev.write_event(&key_ev).unwrap();
                        if ev.value == 0 {
                            // release
                            // send ending EV_SYN
                            let syn_ev = InputEvent {
                                event_type: EventType::EV_SYN,
                                event_code: EventCode::EV_SYN(SYN_REPORT),
                                value: 0,
                                ..ev
                            };
                            keyboard_udev.write_event(&syn_ev).unwrap();

                            button8_on = false;
                        }
                    }
                    InputEvent {
                        event_type: EventType::EV_KEY,
                        event_code: EventCode::EV_KEY(EV_KEY::BTN_EXTRA),
                        value,
                        ..
                    } if value <= 1 => {
                        assert!(button9_on);

                        // send left shift first
                        let mut shift_ev = ev.clone();
                        shift_ev.event_code = EventCode::EV_KEY(EV_KEY::KEY_LEFTSHIFT);
                        keyboard_udev.write_event(&shift_ev).unwrap();

                        // then send left super
                        key_ev.event_code = EventCode::EV_KEY(EV_KEY::KEY_LEFTMETA);
                        keyboard_udev.write_event(&key_ev).unwrap();

                        if ev.value == 0 {
                            // release
                            // send ending EV_SYN
                            let syn_ev = InputEvent {
                                event_type: EventType::EV_SYN,
                                event_code: EventCode::EV_SYN(SYN_REPORT),
                                value: 0,
                                ..ev
                            };
                            keyboard_udev.write_event(&syn_ev).unwrap();

                            button9_on = false;
                        }
                    }
                    _ => {
                        if (button8_on || button9_on) && ev.event_type == EventType::EV_SYN {
                            keyboard_udev.write_event(&key_ev).unwrap();
                        }
                        // forward to mouse_uinput virtual device
                        mouse_udev.write_event(&ev).unwrap();
                    }
                }
            }
        }
    }
}
