// use evdev_sys::input_event;
use input_linux_sys::input_event;
use std::io::Read;

fn main() {
    // let mut mouse = std::fs::File::open("/dev/my_m590_mouse").unwrap();
    let mut keyboard = std::fs::File::open("/dev/my_keyboard/filco_majestouch").unwrap();
    let mut buf = [0u8; 24];
    while let Ok(nbytes) = keyboard.read(&mut buf) {
        assert_eq!(nbytes, 24);
        let e: &input_event = unsafe { std::mem::transmute(&buf) };
        println!("{:?}", e);
    }
    println!("end")
}

